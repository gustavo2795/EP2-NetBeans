/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ep2.oo;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public class DecifraPGM extends Imagem {
 
    private int[][] pixels;
    private byte[] bytes;
    
    public DecifraPGM(String path){
        this.montaImagem(path);
    }
    
    private void setPixels(int height, int width) {
        this.pixels = new int[height][width];
    }

    private void setPixel(int y, int x, int color) {
        this.pixels[y][x] = color;
    }
    
    public int getPixel(int y, int x){
        return this.pixels[y][x];
    }

    private void setBytes() {
        this.bytes = new byte[this.getSize()];
    }

    private void setByte(int pos, byte bb) {
        this.bytes[pos] = bb;
    }

    public byte getByte(int pos) {
        return this.bytes[pos];
    }
    
    
    public static String le_cabecalho(FileInputStream arquivo) {
		String linha = "";
		byte bb;
		try {
			while ((bb = (byte) arquivo.read()) != '\n') {
				linha += (char) bb;
			}
                        
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Linha: " + linha);
		return linha;
    }
    
    public void montaImagem(String caminho){
        this.open(caminho, BufferedImage.TYPE_INT_RGB);
        this.setPixels(this.getHeight(), this.getWidth());
        this.setBytes();
        
        for(int y=0; y<this.getHeight(); y++){
            for(int x=0; x<this.getWidth(); x++){
                try{
                    int color = this.getFile().read();
                    byte bb = (byte) color;
                    this.setByte(this.getWidth()*y +x, bb);
                    
                    if(color<0 || color > this.getMaxGrey()){
                        color=0;
                    }
                    this.setPixel(y, x, color);
                    this.getPicture().setRGB(x,y,new Color(color, color, color).getRGB());
                }catch (Throwable t){
                    t.printStackTrace(System.err);
                }
            }
        }
//        JFrame frame = new JFrame();
//	frame.getContentPane().setLayout(new FlowLayout());
//	frame.getContentPane().add(new JLabel(new ImageIcon(this.getPicture().getScaledInstance(500, 400, 100))));
//	frame.pack();
//	frame.setVisible(true);
    }
    
public static void Decodifica(String file_diretorio) throws FileNotFoundException{
    try {
	FileInputStream arquivo = new FileInputStream(file_diretorio);
	BufferedImage imagem_pgm = null, imagem_pgm_negativo = null;
        System.out.println(file_diretorio  + "diretorio");
        int width = 0;
        int height = 0;
        int maxVal = 0;
	int count = 0;
	byte bb;
        Icon img1, img2;
		
	String linha = DecifraPGM.le_cabecalho(arquivo);
	if("P5".equals(linha)) {
            linha = DecifraPGM.le_cabecalho(arquivo);
            while (linha.startsWith("#")) {
            linha = DecifraPGM.le_cabecalho(arquivo);
            }
            Scanner in = new Scanner(linha); 
            if(in.hasNext() && in.hasNextInt())
                width = in.nextInt();
            else
                System.out.println("Arquivo corrompido");
            if(in.hasNext() && in.hasNextInt())
		height = in.nextInt();
            else
                System.out.println("Arquivo corrompido");
            linha = DecifraPGM.le_cabecalho(arquivo);
            in.close();
            in = new Scanner(linha);
            maxVal = in.nextInt();
            in.close();
				
            imagem_pgm = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
            imagem_pgm_negativo = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
            byte [] pixels = ((DataBufferByte) imagem_pgm.getRaster().getDataBuffer()).getData();
            byte [] pixels_negativo = ((DataBufferByte) imagem_pgm_negativo.getRaster().getDataBuffer()).getData();
				
            while(count < (height*width)) {
		bb = (byte) arquivo.read();
		pixels[count] = bb;
		pixels_negativo[count] = (byte) (maxVal - bb); 
		count++;
            }				
	}
	else {
            System.out.println("Arquivo inválido");
	}
			
	System.out.println("Height=" + height);
	System.out.println("Width=" + width);
	System.out.println("Total de Pixels = " + (width * height));
	System.out.println("Total de Pixels lidos = " + count);
        JFrame frame = new JFrame();
	frame.getContentPane().setLayout(new FlowLayout());
	frame.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm.getScaledInstance(500, 400, 100))));
	frame.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm_negativo.getScaledInstance(500, 400, 100))));
	frame.pack();
	frame.setVisible(true);
        arquivo.close();
    }
    catch(Throwable t) {
	t.printStackTrace(System.err) ;
        return;
    }
}

public void filtro_sharpen(String file_diretorio){
   
    int filter[][] = new int[][]{{-1,-9,-1},{-1,9,-1},{-1,-1,-1}};
    double fator = 0.2;
    
    for(int linha=1;linha<this.getHeight()-1;linha++){
        for(int coluna=1;coluna<this.getWidth()-1;coluna++){
            double valor = 0.0;
            for(int linha_filtro=-1;linha_filtro<2;linha_filtro++){
                for(int col_filtro=-1;col_filtro<2;col_filtro++){
                    valor+=filter[linha_filtro+1][col_filtro+1]*this.pixels[linha+linha_filtro][coluna+col_filtro];
                }
            }
            int pixel = (int)(this.pixels[linha][coluna]-fator*valor);
            pixel = pixel < 0 ? 0: pixel;
            pixel = pixel > 255 ? 255: pixel;
            
            this.getPicture().setRGB(coluna, linha, new Color(pixel, pixel, pixel).getRGB());
            
        }
    }
        JFrame frame = new JFrame();
	frame.getContentPane().setLayout(new FlowLayout());
        //frame.getContentPane().add(new JLabel(new ImageIcon(.getPicture().getScaledInstance(500, 400, 100))));
	frame.getContentPane().add(new JLabel(new ImageIcon(this.getPicture().getScaledInstance(500, 400, 100))));
	frame.pack();
	frame.setVisible(true);
    
}

public void filtro_smooth(String file_diretorio){
   
    int filter[][] = new int[][]{{1,1,1},{1,1,1},{1,1,1}};
    double fator = 0.09;
    
    for(int linha=1;linha<this.getHeight()-1;linha++){
        for(int coluna=1;coluna<this.getWidth()-1;coluna++){
            double valor = 0.0;
            for(int linha_filtro=-1;linha_filtro<2;linha_filtro++){
                for(int col_filtro=-1;col_filtro<2;col_filtro++){
                    valor+=filter[linha_filtro+1][col_filtro+1]*this.pixels[linha+linha_filtro][coluna+col_filtro];
                }
            }
            int pixel = (int)(this.pixels[linha][coluna]-fator*valor);
            pixel = pixel < 0 ? 0: pixel;
            pixel = pixel > 255 ? 255: pixel;
            
            this.getPicture().setRGB(coluna, linha, new Color(pixel, pixel, pixel).getRGB());
            
        }
    }
        JFrame frame = new JFrame();
	frame.getContentPane().setLayout(new FlowLayout());
	frame.getContentPane().add(new JLabel(new ImageIcon(this.getPicture().getScaledInstance(500, 400, 100))));
	frame.pack();
	frame.setVisible(true);
    
}

public void pegarMensagem() {
        char character = 0;
        int count = 1;
        int start;
        String text = "";
        String nova_string = "";
        int aux=0;
        start = Integer.parseInt(this.getComment().replaceAll("[\\D]", ""));

        for (int pos = start; pos < this.getSize(); pos++) {
            character <<= 1;
            character |= this.getByte(pos) & 0x01;
            if (count == 8) {             
                if (character == '#') {   
                    break;
                }
                //
                text+=(char)character;
                count = 0;                
                character = 0;           
            }
            count++;
        }
        System.out.print(text);
        if(text.length()>300){
            for(int i = 0; i<text.length(); i++){
                char c = text.charAt(i);
                nova_string+=(char)c;
                aux++;
                if(aux>299){
                    nova_string = nova_string +"\n";
                    aux = 0;
                }
                
            }
            JOptionPane.showMessageDialog(null, nova_string);
        }else
            JOptionPane.showMessageDialog(null, text);
    }
}
