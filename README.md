# README

## Introdução

  O objetivo deste projeto é uma interface gráfica com a linguagem java capaz de ler arquivos de imagens nos formatos PGM e PPM, além disso, extrair informações embutidas nestas imagens através da técnica de esteganografia.

## O que o software faz

  * Abre imagens no formato **PPM** (coloridas) e **PGM** (preto e branco).
  * Descobre mensagem escondida em formato texto nas imagens PGM.
  * Descobre mensagem escondida em imagens coloridas por meio de aplicação de filtros.
  * Imprime na tela a mensagem descoberta (PGM).
  * Aplica os filtros Negativo, Sharpen e Smooth nas imagens PGM.

## Como compilar

  * É necessário ter o programa NetBeans instalado.
  * Abra o terminal e digite: git init; git clone https://gitlab.com/gustavo2795/EP2-NetBeans.git

## Autor: Gustavo Vieira Braz Gonçalves (140041478)




